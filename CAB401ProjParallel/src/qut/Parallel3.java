package qut;

import qut.*;
import jaligner.*;
import jaligner.matrix.*;
import edu.au.jacobi.pattern.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class Threading3 implements Callable<List<Match>> {
	private GenbankRecord globalGenBank;
	private Gene globalReferenceGene;
	private int numThreadsGlobal;
	private List<Match> consensus;
	private int start;
	private int end;
	
	/**
	 * Creates an object with the data supplied from the main program
	 * @param threadID - ID for the thread, used in calculating thread work
	 * @param genBankRecord - current genbank record being analyzed
	 * @param numThreads - total number of threads for the program, used in calculating thread work
	 * @param referenceGene - current gene being analyzed
	 */
	Threading3(int threadID, GenbankRecord genBankRecord, int numThreads, Gene referenceGene) {
		this.globalGenBank = genBankRecord;
		this.numThreadsGlobal = numThreads;
		this.globalReferenceGene = referenceGene;
		this.consensus = new ArrayList<Match>();
		
		//Calculate work for thread
		int N = globalGenBank.genes.size();
		int chunkSize = (N + (numThreadsGlobal -1)) / numThreadsGlobal;
		start = threadID * chunkSize + 1;
		end = Math.min(start + chunkSize, N);
	}
	
	/**
	 * Override the Callable class call method. Performs the work on the assigned thread
	 */
	@Override
	public List<Match> call() throws Exception {
		//Run the loop with the calculated amount of work for the thread
		for (int i = start; i < end; i++) {
			Gene gene = globalGenBank.genes.get(i);
			if (Parallel.Homologous(gene.sequence, globalReferenceGene.sequence)) {
				NucleotideSequence upStreamRegion;
				Match prediction;
				//Intrinsic lock globalGenBank to prevent race condition
				synchronized(globalGenBank) {
					upStreamRegion = Parallel.GetUpstreamRegion(globalGenBank.nucleotides, gene);
					prediction = Parallel.PredictPromoter(upStreamRegion);
				}
				if (prediction != null) {
					consensus.add(prediction);
				}
			}
		}
		return consensus;
	}
}

public class Parallel3
{
    private static HashMap<String, Sigma70Consensus> consensus = new HashMap<String, Sigma70Consensus>();
    private static Series sigma70_pattern = Sigma70Definition.getSeriesAll_Unanchored(0.7);
    private static final Matrix BLOSUM_62 = BLOSUM62.Load();
    private static byte[] complement = new byte['z'];
    private static final int numThreadsGlobal = 15;

    static
    {
        complement['C'] = 'G'; complement['c'] = 'g';
        complement['G'] = 'C'; complement['g'] = 'c';
        complement['T'] = 'A'; complement['t'] = 'a';
        complement['A'] = 'T'; complement['a'] = 't';
    }
                    
    private static List<Gene> ParseReferenceGenes(String referenceFile) throws FileNotFoundException, IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(referenceFile)));
        List<Gene> referenceGenes = new ArrayList<Gene>();
        while (true)
        {
            String name = reader.readLine();
            if (name == null)
                break;
            String sequence = reader.readLine();
            referenceGenes.add(new Gene(name, 0, 0, sequence));
            consensus.put(name, new Sigma70Consensus());
        }
        consensus.put("all", new Sigma70Consensus());
        reader.close();
        return referenceGenes;
    }

    public static boolean Homologous(PeptideSequence A, PeptideSequence B)
    {
        return SmithWatermanGotoh.align(new Sequence(A.toString()), new Sequence(B.toString()), BLOSUM_62, 10f, 0.5f).calculateScore() >= 60;
    }

    public static NucleotideSequence GetUpstreamRegion(NucleotideSequence dna, Gene gene)
    {
        int upStreamDistance = 250;
        if (gene.location < upStreamDistance)
           upStreamDistance = gene.location-1;

        if (gene.strand == 1)
            return new NucleotideSequence(java.util.Arrays.copyOfRange(dna.bytes, gene.location-upStreamDistance-1, gene.location-1));
        else
        {
            byte[] result = new byte[upStreamDistance];
            int reverseStart = dna.bytes.length - gene.location + upStreamDistance;
            for (int i=0; i<upStreamDistance; i++)
                result[i] = complement[dna.bytes[reverseStart-i]];
            return new NucleotideSequence(result);
        }
    }

    public static Match PredictPromoter(NucleotideSequence upStreamRegion)
    {
        return BioPatterns.getBestMatch(sigma70_pattern, upStreamRegion.toString());
    }

    private static void ProcessDir(List<String> list, File dir)
    {
        if (dir.exists())
            for (File file : dir.listFiles())
                if (file.isDirectory())
                    ProcessDir(list, file);
                else
                    list.add(file.getPath());
    }

    private static List<String> ListGenbankFiles(String dir)
    {
        List<String> list = new ArrayList<String>();
        ProcessDir(list, new File(dir));
        return list;
    }

    private static GenbankRecord Parse(String file) throws IOException
    {
        GenbankRecord record = new GenbankRecord();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        record.Parse(reader);
        reader.close();
        return record;
    }

    public static void run(String referenceFile, String dir) throws FileNotFoundException, IOException
    {   
    	List<Gene> referenceGenes = ParseReferenceGenes(referenceFile);
    	
    	//Instantiate the pool of threads
        ExecutorService pool = Executors.newFixedThreadPool(numThreadsGlobal);
        for (String filename : ListGenbankFiles(dir)) {
        	System.out.println(filename);
            GenbankRecord globalGenBank = Parse(filename);
            for (Gene referenceGene : referenceGenes) {
            	Gene globalReferenceGene = referenceGene;
            	System.out.println(referenceGene.name);
				
            	//Assign work to a thread in the pool
            	List<Future<List<Match>>> list = new ArrayList<Future<List<Match>>>(numThreadsGlobal);
				for(int i = 0; i < numThreadsGlobal; i ++) {
					Future<List<Match>> results = pool.submit(new Threading3(i, globalGenBank, numThreadsGlobal, globalReferenceGene));
					list.add(results);
				}
				
				//Wait for threads to finish execution and return results
				List<List<Match>> matchesLists = new ArrayList<List<Match>>(numThreadsGlobal);
				for(int i = 0; i < numThreadsGlobal; i ++) {
					List<Match> matches = new ArrayList<Match>();
					try {
						matches = list.get(i).get();
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
					matchesLists.add(matches);
				}
				
				//Add results to consensus
				for(int i = 0; i < matchesLists.size(); i++) {
					for(Match match : matchesLists.get(i)) {
						consensus.get(globalReferenceGene.name).addMatch(match);
		               	consensus.get("all").addMatch(match);
					}
				}
            }
        }
        
        //Write results to file
        FileWriter fr = new FileWriter("resultsParallel3Threads" + numThreadsGlobal + ".txt");
		BufferedWriter bufferedWriter = new BufferedWriter(fr);
        for (Map.Entry<String, Sigma70Consensus> entry : consensus.entrySet()) {
        	System.out.println(entry.getKey() + " " + entry.getValue());
			bufferedWriter.write(entry.getKey() + " " + entry.getValue());
        	bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }


    public static void main(String[] args) throws FileNotFoundException, IOException
    {
    	long start = System.currentTimeMillis();
        run("referenceGenes.list", "C:/Users/Peginis/git/cab401-project/CAB401ProjParallel/Ecoli");
        long end = System.currentTimeMillis();
        long timeElapsed = end - start;
        System.out.println(timeElapsed);
    }
}
